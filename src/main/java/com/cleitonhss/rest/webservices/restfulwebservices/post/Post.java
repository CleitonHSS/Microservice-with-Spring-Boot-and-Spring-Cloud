package com.cleitonhss.rest.webservices.restfulwebservices.post;

import java.util.Date;

import com.cleitonhss.rest.webservices.restfulwebservices.user.User;

public class Post {

    private Integer id;
    private User author;
    private String title;
    private String description;
    private Date createDate;

    protected Post(){

    }

    public Post(Integer id, User author, String title, String description, Date createDate) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.description = description;
        this.createDate = createDate;
    }
    
	public void setId(Integer id) {
        this.id = id;
	}

	public Integer getId() {
		return this.id;
    }

    public void setAuthor(User author) {
        this.author = author;
	}

	public User getAuthor() {
		return this.author;
    }
    
    public void setTitle(String title) {
        this.title = title;
	}

	public String getTitle() {
		return this.title;
    }

    public void setDescription(String description) {
        this.description = description;
	}

	public String getDescription() {
		return this.description;
    }
    
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
	}

	public Date getCreateDate() {
		return this.createDate;
	}
}