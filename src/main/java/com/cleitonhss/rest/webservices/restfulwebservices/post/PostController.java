package com.cleitonhss.rest.webservices.restfulwebservices.post;

import java.net.URI;
import java.util.List;

import com.cleitonhss.rest.webservices.restfulwebservices.exception.UserNotFoundException;
import com.cleitonhss.rest.webservices.restfulwebservices.user.UserDaoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class PostController {

    @Autowired
    private PostDaoService service;
    
    @GetMapping("/users/{id}/posts")
    public List<Post> retrieveAllUsers(@PathVariable int id) {
        return service.findAll(id);
    }

    @GetMapping("/users/{id}/posts/{id_post}")
    public Post retrieveUserById(@PathVariable int id, @PathVariable int id_post) {
        Post post = service.findByid(id, id_post);
        if(post==null){
            throw new UserNotFoundException("id: "+ id + " id_post: "+id_post);
        }
        return post;
    }

    @PostMapping("/users/posts")
    public ResponseEntity<Object> createPost(@RequestBody Post post, @RequestHeader int authorId) {
        //int autId = Integer.parseInt(authorId.trim());
        post.setAuthor(new UserDaoService().findOne(authorId));
        Post savedPost = service.save(post);
        URI location = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(savedPost.getId())
            .toUri();
        
        return ResponseEntity.created(location).build();
    }
}