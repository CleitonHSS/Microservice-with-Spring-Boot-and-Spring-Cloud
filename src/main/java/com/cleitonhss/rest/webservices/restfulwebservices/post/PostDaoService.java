package com.cleitonhss.rest.webservices.restfulwebservices.post;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cleitonhss.rest.webservices.restfulwebservices.user.UserDaoService;

import org.springframework.stereotype.Component;

@Component
public class PostDaoService {

    private static List<Post> posts = new ArrayList<>();
    private static Integer usersCount = 3;

    static{
        posts.add(new Post(1, new UserDaoService().findOne(1),"Post 1","Hello post 1!",new Date()));
        posts.add(new Post(2, new UserDaoService().findOne(1),"Post 2","Hello post 2!",new Date()));
        posts.add(new Post(3, new UserDaoService().findOne(2),"Post 3","Hello post 3!",new Date()));
    }

    public List<Post> findAll(int authorId){
        List<Post> resulteList = new ArrayList<>();
        for (final Post post : posts) {
            if (post.getAuthor().getId() == authorId) {
                resulteList.add(post);
            }
        }
        return resulteList;
    }

    public Post findByid(int authorId, int id) {
        for (final Post post : posts) {
            if (post.getAuthor().getId() == authorId && post.getId() == id) {
                return post;
            }
        }
        return null;
    }

    public Post save(Post post) {
        if(post.getId()==null){
            post.setId(++usersCount);
        }
        posts.add(post);
        return post;
    }

}